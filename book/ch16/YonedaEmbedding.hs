
-- 1, express the co-Yoneda embedding in Haskell
-- (x -> a) -> (x -> b) -> (a -> b)
atob :: a -> b
atob a = undefined

fromX :: (x -> a) -> x -> b
fromX f = atob . f

-- 2, show that the bijection between fromY and btoA is an isomorphism
-- (a -> x) -> (b -> x) -> (b -> a)
btoa :: b -> a
btoa b = undefined

fromY :: (a -> x) -> b -> x
fromY f b = f (btoa b)
-- fromY f = f . btoa

f1 :: (a -> x) -> a
f1 = btoa . fromY
f2 :: a -> (a -> x)
f2 = fromY . btoa

{-
In terms of evaluation functors, we take objects a and b in C
and show that their evaluations are equal
(a, b) -> [C, Set](a^, b^) == (b -> a)
The target functor in the isomorphism is the evaluation functor,
(a, b) -> (b -> a)
-}
