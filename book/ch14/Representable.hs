{-# LANGUAGE TypeFamilies #-}

import GHC.Types (Any)

-- Chapter 14, representable functors

-- 2 Show that Maybe is not representable
alphaMaybe :: (Int -> x) -> Maybe x
alphaMaybe f = Just (f 12)

betaMaybe :: Maybe x -> (Int -> x)
betaMaybe (Just x) = const x
betaMaybe Nothing = undefined -- I don't know how to return (Int -> x) here, same problem as we had with the head of a list

-- 3 Is the Reader functor representable?
-- The Reader functor is the hom-functor in Haskell
newtype Reader e a = Reader {func :: e -> a}

instance Functor (Reader e) where
  fmap f (Reader g) = Reader (f . g)

alphaReader :: (a -> x) -> Reader a x
alphaReader = Reader

betaReader :: Reader a x -> (a -> x)
betaReader = func

-- Looks good!

-- 4 Using 'Stream' representation, memoize a function that squares its argument.
class Representable f where
  type Rep f :: *
  tabulate :: (Rep f -> x) -> f x
  index :: f x -> Rep f -> x

data Stream x = Cons x (Stream x)

instance Representable Stream where
  type Rep Stream = Integer
  tabulate f = Cons (f 0) (tabulate (f . (+ 1)))
  index (Cons b bs) n = if n == 0 then b else index bs (n - 1)

squares :: Integer -> Integer
squares x = x * x

memoSquares :: Stream Integer
memoSquares = tabulate squares

-- Use: index memoSquares 3

-- 6 Pair representability
data Pair a = Pair a a deriving (Show)

instance Functor Pair where
  fmap f (Pair x y) = Pair (f x) (f y)

instance Representable Pair where
  type Rep Pair = Bool
  tabulate f = Pair (f True) (f False)
  index (Pair x x') b
    | b = x
    | otherwise = x'
