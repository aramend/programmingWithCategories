-- Chapter 6
-- Simple Algebraic Data Types
module Algebra where

-- 1 Isomorphism between Maybe a and Either () a
-- Maybe to Unit Either
m2ue :: Maybe a -> Either () a
m2ue Nothing  = Left ()
m2ue (Just x) = Right x

ue2m :: Either () a -> Maybe a
ue2m (Left ()) = Nothing
ue2m (Right x) = Just x

-- 5 Show that a + a = 2 * a in the algebra of types
data Sum a = First a | Second a
-- First a | Second a = ((True | False), a)
-- Show isomorphism between both sides of =
-- sum to prod
s2p :: Sum a -> (Bool, a)
s2p (First x) = (True, x)
s2p (Second x) = (False, x)

p2s :: (Bool, a) -> Sum a
p2s (True, x) = (First x)
p2s (False, x) = (Second x)
