{-
Use Maybe for this
-}
module Partial where

-- 1 Kleisli category for partial functions
-- ..Monads...
composePartial :: (a -> Maybe b) -> (b -> Maybe c) -> (a -> Maybe c)
composePartial f1 f2 = \x -> (f1 x) >>= f2

idPartial :: a -> Maybe a
idPartial = Just

-- 2 Safe reciprocal function
safeReciprocal :: (Ord a, Fractional a) => a -> Maybe a
safeReciprocal x
  | x > 0     = Just $ recip x
  | otherwise = Nothing

-- 3
safeRoot :: (Ord a, Floating a) => a -> Maybe a
safeRoot x
  | x > 0     = Just $ sqrt x
  | otherwise = Nothing

safeRootReciprocal :: (Ord a, Floating a) => a -> Maybe a
safeRootReciprocal = composePartial safeReciprocal safeRoot
