{-
The Writer implementation from 4.2 (page 32)
but use 'data' instead of 'type'
-}
module Writer where

import Data.Char (toUpper)

data Writer a = Writer a String

-- Identity for data
return' :: a -> Writer a
return' x = Writer x ""

(>=>) :: (a -> Writer b) -> (b -> Writer c) -> (a -> Writer c)
m1 >=> m2 = \x ->
  let Writer y s1 = m1 x
      Writer z s2 = m2 y
  in Writer z $ s1 ++ s2

upCase :: String -> Writer String
upCase s = Writer (map toUpper s) "upCase "

toWords :: String -> Writer [String]
toWords s = Writer (words s) "toWords "

process :: String -> Writer [String]
process = upCase >=> toWords

-- Try this out
main :: IO ()
main = do
  let sentence = "I love life."
  putStrLn "Input:"
  print sentence
  putStr "\n"
  let Writer res trace = process sentence
  putStrLn "Output:"
  print res
  print trace
