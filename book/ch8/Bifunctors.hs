module Bifunctors where

import Data.Functor.Const

class Bifunctor f where
  bimap :: (a -> c) -> (b -> d) -> f a b -> f c d
  bimap g h = first g . second h
  first :: (a -> c) -> f a b -> f c b
  first g = bimap g id
  second :: (b -> d) -> (f a b) -> f a d
  second = bimap id

-- 1 Show that Pair is a bifunctor
data Pair a b = Pair a b deriving (Eq, Show)

instance Bifunctor Pair where
  bimap g h (Pair x y) = Pair (g x) (h y)
  first f (Pair x y) = Pair (f x) y
  second f (Pair x y) = Pair x (f y)

bfCheck :: (Bifunctor f, Eq (f a b), Eq a, Eq b, Num a, Num b) => f a b -> Bool
bfCheck x = (bimap g h x) == (first g . second h) x where
  g = (+1)
  h = (*4)

-- 2 Maybe isomorphism
data Identity a = Identity a deriving Show

mToE :: Maybe a -> Either (Const () a) (Identity a)
mToE (Just x)  = Right $ Identity x
mToE (Nothing) = Left $ Const ()

eToM :: Either (Const () a) (Identity a) -> Maybe a
eToM (Left (Const ())) = Nothing
eToM (Right (Identity x)) = Just x

-- 3 Prelist bifunctor
data Prelist a b = Nil | Cons a b deriving (Eq, Show)

instance Bifunctor Prelist where
  bimap _ _ Nil = Nil
  bimap g h (Cons x y) = Cons (g x) (h y)

-- 4 Show that the following define bifunctors in a and b
data K2 c a b = K2 c deriving (Eq, Show)
data Fst a b = Fst a deriving (Eq, Show)
data Snd a b = Snd b deriving (Eq, Show)

instance Bifunctor (K2 c) where
  bimap _ _ (K2 x) = K2 x

instance Bifunctor Fst where
  bimap g _ (Fst x) = Fst (g x)

instance Bifunctor Snd where
  bimap _ h (Snd x) = Snd (h x)

-- Print results for all checks
main :: IO ()
main = do
  let p = Pair 1 2
  putStrLn $ "bimap of " ++ show p ++ " is equal to a composition of first and second of the same: " ++ show (bfCheck p)
  let pre1 = (Nil :: Prelist Int Int)
  let pre2 = Cons 3 10
  putStrLn $ "bimap of " ++ show pre1 ++ " is equal to a composition of first and second of the same: " ++ show (bfCheck pre1)
  putStrLn $ "bimap of " ++ show pre2 ++ " is equal to a composition of first and second of the same: " ++ show (bfCheck pre2)
  let k2 = K2 4
  let first = Fst 86
  let second = Snd 24
  putStrLn $ "bimap of " ++ show k2 ++ " is equal to a composition of first and second of the same: " ++ show (bfCheck k2)
  putStrLn $ "bimap of " ++ show first ++ " is equal to a composition of first and second of the same: " ++ show (bfCheck first)
  putStrLn $ "bimap of " ++ show second ++ " is equal to a composition of first and second of the same: " ++ show (bfCheck second)
