module NatTrans where

-- 1 Natural transformation from Maybe to List
maybeToList :: Maybe a -> [a]
maybeToList Nothing  = []
maybeToList (Just x) = [x]

-- There are two commuting squares for the sum type of Maybe
-- Nothing
commutesNothing :: Bool
commutesNothing =
  maybeToList (fmap f Nothing) == fmap f (maybeToList Nothing) where
    f = (+2) :: Int -> Int

-- Just a
commutesJust :: Bool
commutesJust =
  maybeToList (fmap f (Just x)) == fmap f (maybeToList (Just x)) where
    f = (+2) :: Int -> Int
    x = 4


-- 2 Natural transformations between Reader () and List
newtype Reader e a = Reader {func :: e -> a}

instance Functor (Reader e) where
  fmap f (Reader g) = Reader (f . g)

-- From the book
dumb :: Reader () a -> [a]
dumb _ = []

obvious :: Reader () a -> [a]
obvious r = [func r ()]

unitFunction :: () -> Int
unitFunction () = 5

commutesDumb :: Bool
commutesDumb =
  dumb (fmap f r) == fmap f (dumb r) where
    r = Reader unitFunction
    f = (+2) :: Int -> Int

commutesObvious :: Bool
commutesObvious =
  obvious (fmap f r) == fmap f (obvious r) where
    r = Reader unitFunction
    f = (+2) :: Int -> Int

-- There are infinitely many natural transformations between Reader () and List,
-- since I on line 35 I could haver written [func r (), func r ()], and so on.

-- 3 Natural transformation between Reader Bool and Maybe
dumb' :: Reader Bool a -> Maybe a
dumb' _ = Nothing

true :: Reader Bool a -> Maybe a
true r = Just (func r True)

false :: Reader Bool a -> Maybe a
false r = Just (func r False)

result :: [String]
result =
  [
    "Commuting square for maybeToList: Nothing",
    show commutesNothing,
    "Commuting square for maybeToList: Just a",
    show commutesJust
  ]

main :: IO ()
main = do
  putStrLn $ unlines result
