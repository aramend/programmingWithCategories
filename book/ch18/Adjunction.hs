-- Define the Adjunction between a product object and function object in Haskell
-- To make this look like an adjunction, write a mapping between hom-sets

pToF :: ((z, a) -> b) -> (z -> (a -> b))
pToF = curry

fToP :: (z -> (a -> b)) -> ((z, a) -> b)
fToP = uncurry

-- Huh, whaddaya know!
