{
  description = "A shell for Haskell development";
  inputs.nixpkgs.url = github:NixOS/nixpkgs/master;

  outputs = { self, nixpkgs }:
    let
      # Access packages from old nixpkgs
      pkgs = nixpkgs.legacyPackages.x86_64-linux;
      hs = pkgs.haskellPackages;

      # Compiler with packages
      ghc = hs.ghcWithPackages (ps: with ps; [
        unordered-containers
        hspec
        QuickCheck
        checkers
        # category-extras # -> broken, fix?
      ]);

      # Haskell tooling
      haskellEnv = (with hs; [
        cabal-install
        hlint
        ghcid
        cabal2nix
      ]);


    in {
      devShell.x86_64-linux =
      pkgs.stdenv.mkDerivation {
        name = "haskell-devel";
        buildInputs = [
          ghc
          haskellEnv
        ];
      };
    };
}
